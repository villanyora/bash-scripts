#!/bin/bash
#   @author 2023 Miklós Balázs <villanyora@yahoo.co.uk>
#
#	SPDX-License-Identifier: GPL-2.0-or-later
echo "Enter the full path to directory of the raw GoPro videos:"
read path
    if [[ "${path:0:2}" == "~/" ]]                      #check if the home directory was entered as "~"
    then
    path="$HOME/${path:2}"
    fi
    if [[ "${path: -1}" != "/" ]]                       #check if the path ends with "/"
    then
    path="$path/"
    fi
    if ! [ -d $path ]                                   #check if the directory exists at all
    then
        echo -e "The directory:\n$path\ndoesn't exit. Check the spelling!\nGood bye!" && exit 1
    fi
#Before going any further check first, if there are any "GOPR*.mp4" file(s) to work on at all.
for F in $path"GOPR"*.MP4; do                          #Only the first files start with "GOPR"
    if ! [ -f $F ]                                     #Check if any such file exists at all.
    then
        echo -e "There are no \".MP4\" files starting with \"GOPR\" in:\n$path\nThe extension is case sensitive (change \".mp4\" to \".MP4\")!\nCheck the spelling! Good bye!" && exit 1
    fi
done
#All good, keep going.
echo -e "Enter the 2 or 3 letter iso code for language\n(e.g. en or eng, es or spa, fr or fra, de or ger, etc...)"
read lang
echo -e "Enter the name of the TRACK (not the file)\n(e.g. Skitrip, Biketour, Holiday, etc...)"
read name
echo "Started running at $(date '+%H':'%M':'%S')"
for F in $path"GOPR"*.MP4; do                          #Only the first files start with "GOPR"
    i=1
    input="( "$F" )"                                       #Add the original file
        while [ -f $path"GP0"$i${F:(( ${#F}-8 ))} ]; do    #Check if any additional video-segment(s) exist(s)
        input+=" + ( "$path"GP0"$i${F:(( ${#F}-8 ))}" )"   #Add the new/next segment
        (( i+= 1 ))
        done
        echo "$(date '+%H':'%M':'%S') Converting $F into ${path}${F:(( ${#F}-8 )):4} ($(date '+%Y')).mkv"
        if echo "$input" | grep -q " + "                   #Check if it's a merge'
        then
        echo "Merging: $input"                             #Info about the merged files
        fi
    #Start the conversion/merge
    /usr/bin/mkvmerge --output ${path}${F:(( ${#F}-8 )):4}" ($(date '+%Y')).mkv" --language 0:${lang} --track-name 0:${name} --language 1:${lang} --track-name 1:${name} ${input} -q
    echo "...done"
done
echo "$(date '+%H':'%M':'%S') All done, check your files!"
exit 0
