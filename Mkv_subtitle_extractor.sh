#!/bin/bash
#   @author 2023 Miklós Balázs <villanyora@yahoo.co.uk>
#
#	SPDX-License-Identifier: GPL-2.0-or-later
#   To make this file an executable one, type "chmod +x subt_extractor.sh" in the terminal
#
#   By default "only" the current directory is checked for mkv files
#   If you have multiple files in several subdirectories, put this file
#   into the root dir and change "for F in ./*.mkv" in line 11 to "for F in ./*/*.mkv" (and save it)
#   to go through ALL .mkv-s in the subdirectories (e.g season1, season2, part1, partx, etc...).
for F in ./*.mkv;                                                               
do
echo -e '\nStarting with episode: '${F}'\n'
aid=$(mkvmerge -i "$F" | grep "subtitles" | cut -d " " -f3 | tr -d ":")   #get the Track ID (TID) of each subtitle track in the mkv
    for tid in $aid;                                                      #loop through each TID in the "array" (aid = ArrayID)
    do
#   In order to avoid reading data for a different TID
#   limit the output of mkvmerge -J to the actual TID,
#   with grep -A $rng (number of lines to show (-A)fter
#   the found expression: "id": ${tid},
    ntid=$(($tid + 1))                                                   #the next/one_higher TID
    pcurr=$(mkvmerge -J "$F" | grep -n "\"id\": ${tid}" | cut -d ":" -f1)     #get to position/line_number of this TID
    pnext=$(mkvmerge -J "$F" | grep -n "\"id\": ${ntid}," | cut -d ":" -f1)    #get to position/line_number of the next TID
    if [[ $pnext ]]
        then
        rng=$(($pnext - $pcurr - 4))                                    #calc the Nr. of lines in mkvmerge -J for the current TID
        else
        rng=18                                                          #if there's no higher TID (e.i. next track) show max. 18 lines
    fi
    lang=$(mkvmerge -J "$F" | grep -A $rng "\"id\": ${tid}," | grep language\" | cut -d ":" -f2 | cut -d "\"" -f2 | cut -d "\"" -f1)            #get the language of the track
    type=$(mkvmerge -J "$F" | grep -A $rng "\"id\": ${tid}," | grep codec_id | cut -d ":" -f2 | cut -d "\"" -f2 | cut -d "\"" -f1)              #get the Codec type
    forced=$(mkvmerge -J "$F" | grep -A $rng "\"id\": ${tid}," | grep forced_track | cut -d ":" -f2 | cut -d " " -f2 | cut -d "," -f1)          #check if it's a forced track
    if [[ -z $forced ]] || [[ "$forced" == "false" ]]                                      #check if it's "set" in the Name field
        then
        name=$(mkvmerge -J "$F" | grep -A $rng "\"id\": ${tid}," | grep track_name | tr A-Z a-z | grep "forced" -o)
        if [[ "$name" == "forced" ]]
            then
                ext3=".forced"
                unset name
            else
                ext3=""
        fi
    elif [[ $forced == "true" ]]
        then
            ext3=".forced"
    fi
    
    SDH=$(mkvmerge -J "$F" | grep -A $rng "\"id\": ${tid})" | grep "flag_hearing_impaired" | cut -d ":" -f2 | cut -d " " -f2 | cut -d "," -f1)
    if  [[ -z $SDH ]] || [[ "$SDH" == "false" ]]                                            #check if it's "set" in the Name field
        then
        name=$(mkvmerge -J "$F" | grep -A $rng "\"id\": ${tid}," | grep track_name | tr A-Z a-z | grep "sdh" -o)
        if [[ "$name" == "sdh" ]]                                                           #searching for "hi" is too "risky"
            then
                ext2=".sdh"
                unset name
            else
                ext2=""
        fi
    elif [[ "$SDH" == "true" ]]
        then
            ext2=".sdh"
    fi
    if [[ -z $lang ]]                                                                         #still no language :( As a desperate try:
        then                                                                                  #Name field's first word's first 2-3 letters
            lang=$(mkvmerge -J "$F" | grep -A $rng "\"id\": ${tid}," | grep track_name | tr A-Z a-z | cut -d ":" -f2 | cut -d "\"" -f2 | cut -d " " -f1)
            if  ((${#lang} > 3)); then lang=${lang:0:3}; fi                                 #cut back to the length of 3
    fi
    case $lang in                                                                           #set the ISO 639-2/T code for the language
        "en"|"eng") ext1=".eng";;
        "hu"|"hun"|"mag"|"ung") ext1=".hun";;
        "de"|"deu"|"ger") ext1=".ger";;
        "fr"|"fra"|"fre") ext1=".fra";;
        "es"|"spa"|"esp") ext1=".spa";;
        "ja"|"jpn"|"jap") ext1=".jpn";;
        "it"|"ita") ext1=".ita";;
        "nl"|"nld"|"dut") ext1=".dut";;
        "no"|"nor") ext1=".nor";;
        "fi"|"fin") ext1=".fin";;
        "sv"|"swe") ext1=".swe";;
        "da"|"dan") ext1=".dan";;
        "pl"|"pol") ext1=".pol";;
        "cs"|"ces"|"cze") ext1=".cze";;
        "el"|"ell"|"gre") ext1=".gre";;
        "pt"|"por") ext1=".por";;
        "ro"|"ron"|"rum"|"rom") ext1=".ron";;
        "tr"|"tur") ext1=".tur";;
        "zh"|"zho"|"chi") ext1=".chi";;
        *) ext1=".und";;                                                                            #The language is UNDetermined :(
    esac
    case $type in                                                                                   #set the type extension (last one)
        "S_TEXT/UTF8") ext4=".srt";;
        "S_HDMV/PGS") ext4=".sup";;
        "S_TEXT/ASS") ext4=".ass";;
        "S_VOBSUB") ext4=".sub";;
        "S_TEXT/USF") ext4=".usf";;
    esac
#    echo "Extracting track ${tid} as ${F:0: -4}${ext1}${ext2}${ext3}${ext4}"                                         #this line was for testing purposes

    if [[ -f ${F:0: -4}${ext1}${ext2}${ext3}${ext4} ]]
        then
            mkvextract "${F}" tracks "$tid:${F:0: -4}${ext1}${ext2}${ext3}.track${tid}${ext4}"                       #In case no language or no flag (forced, sdh) is set
        else                                                                                                        #for the track, add its TID for a unique filename
            mkvextract "${F}" tracks "$tid:${F:0: -4}${ext1}${ext2}${ext3}${ext4}"
    fi
    unset type lang forced SDH pcurr pnext name ext1 ext2 ext3 ext4
    done
done
echo "We're all done ... Thank you for using MKV Subtitle Extractor!"
exit 0