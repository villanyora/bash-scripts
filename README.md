# Bash Scripts

In case you're a Linux user (like me), you may want to speed up/automate repetitive, boring tasks that should be executed for several files manually. Below are a few scripts to help you avoid that for some _very specific but relatively common_ cases.

# **1. GoProMergeConv.sh**

Are you a GoPro-fan? Do you take it to your skitrip, biketour, holiday or whereever ending up with several raw GoPro files on your harddrive? Are many of them splitted into chunks because of the 2GB limit of your FAT32 microSD? This could help you to spare a big part of the editing.

GoProMergeConv.sh will ask for the location of your raw GoPro files, and then go through that directory:
- taking each raw GOPR*.mp4 file to convert it to .mkv
- checking if a file has any additional chunks (up to 10) and merges them with the base file into one .mkv file
- sets the language of both (video and audio) tracks to the entered one
- sets the name for both (video and audio) tracks to the entered one (this is NOT the filename)
- adds the current year at the end of the filename and removes the GOPR part from the front (e.g. 0154 (2023).mkv)

## Dependencies
1. Bash installed
2. mkvtoolnix installed (optionally you can also install mkvtoolnix-gui for a visual helper with the files)

## Usage
1. Open the directory of the stored GoProMergeConv.sh in terminal and enter the command `chmod +x GoProMergeConv.sh` to make the file an executable one.
2. Enter `./GoProMergeConv.sh` to run the script.

## Support
In case you have any problems/issues/(wishes) let me know here.

## License
SPDX-License-Identifier: GPL-2.0-or-later

# **2. Mkv_subtitle_extractor.sh**

Are you a TV-Show/series fan? Don't like the subtitles embeded in the mkv video-container? Want to get them _all_ out of _every_ .mkv file so you can edit them? Here's a script for you to speed that up.

Mkv_subtitle_extractor.sh will go through every .mkv file in the current directory or subdirectories (see the comments in the script for that)
and get them all out. While doing so it will:
- check the language of each subtitle track and set it in the extension accordingly. (e.g. movie.eng.srt or movie.und.srt if no language is set)
- check the codec of each subtitle to set the extension accordingly (e.g. .srt, .sub, .sup, .ass, etc...)
- check the sdh flag for each subtitle to include (if it's set) in the filename (e.g. movie.eng.sdh.srt)
- check the forced flag for each subtitle to include (if it's set) in the filename (e.g. movie.eng.sdh.forced.srt)
- will add the track ID of each subtitle in the filename - if needed - to avoid overwriting similar ones within the same .mkv (e.g. movie.track3.srt)

If the Language is not set anywhere, the field "track_name" will be checked (first 2-3 letters)

If the Forced-flag is not set, the field "track_name" will be checked for a (case independent) "forced" entry

If the Hearing-impaired-flag is not set, the field "track_name" will be checked for a (case independent) "sdh" entry


## Dependencies
1. Bash installed
2. mkvtoolnix installed (optionally you can also install mkvtoolnix-gui for a visual helper with the files)

## Usage
1. Open the directory of the stored Mkv_subtitle_extractor.sh in terminal and enter the command `chmod +x Mkv_subtitle_extractor.sh` to make the file an executable one.
2. If you have the episodes arranged in subdirectories (e.g. MyTVShow/Season1/eps*.mkv, MyTVShow/Season2/eps*.mkv, etc...) open Subt_extractor.sh for editing and modify the  `for F in ./*mkv;` entry in line 11 to `for F in ./*/*.mkv;` and save the file. Move/copy the file into the root directory (e.g. MyTvShow/) and open/change to the directory in terminal.
3. Enter `./Mkv_subtitle_extractor.sh` to run the script.

## Support
In case you have any problems/issues/(wishes) let me know here.

## License
SPDX-License-Identifier: GPL-2.0-or-later

Enjoy!
